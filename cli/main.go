package main

import (
	"github.com/Sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.cern.ch/flutter/go-proxy"
	"gitlab.cern.ch/fts/go-fts"
)

var (
	debug   bool
	context fts.Context
)

var rootCmd = &cobra.Command{
	PersistentPreRun: func(cmd *cobra.Command, args []string) {
		if debug {
			logrus.SetLevel(logrus.DebugLevel)
		}

		var err error
		if context.UserCert == "" && context.UserKey == "" {
			context.UserCert, context.UserKey, err = proxy.GetCertAndKeyLocation()
		} else if context.UserKey == "" {
			context.UserKey = context.UserCert
		} else if context.UserCert == "" {
			context.UserCert = context.UserKey
		}

		if err != nil {
			logrus.Fatal(err)
		}

		logrus.Debug("Endpoint: ", context.Endpoint)
		logrus.Debug("Certificate: ", context.UserCert)
		logrus.Debug("Private key: ", context.UserKey)
	},
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Usage()
	},
}

func init() {
	flags := rootCmd.PersistentFlags()

	flags.BoolVar(&debug, "debug", false, "Debug output")
	flags.StringVar(&context.Endpoint, "endpoint", "https://fts3-devel.cern.ch:8446", "FTS endpoint")
	flags.StringVar(&context.CaPath, "capath", "/etc/grid-security/certificates", "CA PAth")
	flags.StringVar(&context.UserCert, "cert", "", "User certificate")
	flags.StringVar(&context.UserKey, "key", "", "Use private key")
	flags.BoolVar(&context.Insecure, "insecure", false, "Do not verify peer certificate")
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		logrus.Fatal(err)
	}
}
