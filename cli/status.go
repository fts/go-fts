package main

import (
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/spf13/cobra"
)

var statusCmd = &cobra.Command{
	Use: "status",
	Run: func(cmd *cobra.Command, args []string) {
		for _, jobID := range args {
			job, err := context.GetJobStatus(jobID, true)
			if err != nil {
				logrus.Fatal(err)
			}
			fmt.Println(job, "\n")
		}
	},
}

func init() {
	rootCmd.AddCommand(statusCmd)
}
