package main

import (
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.cern.ch/fts/go-fts"
	"time"
)

var submitCmd = &cobra.Command{
	Use: "submit",
	Run: func(cmd *cobra.Command, args []string) {
		job := fts.Submission{}

		if len(args) < 2 {
			logrus.Fatal("Missing source and/or destination URL")
		}

		job.Files = []fts.SubmissionFile{fts.SubmissionFile{
			Sources:      []string{args[0]},
			Destinations: []string{args[1]},
		}}
		if len(args) > 2 {
			job.Files[0].Checksum = args[2]
			job.Params.VerifyChecksum = fts.ChecksumEnd2End
		}

		dlgId, err := context.Delegate(6 * time.Hour)
		if err != nil {
			logrus.Fatal(err)
		}
		logrus.Info("Delegation ID: ", dlgId)

		jobId, err := context.Submit(&job)
		if err != nil {
			logrus.Fatal(err)
		}
		fmt.Println(jobId)
	},
}

func init() {
	rootCmd.AddCommand(submitCmd)
}
