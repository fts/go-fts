package main

import (
	"fmt"
	"github.com/Sirupsen/logrus"
	"github.com/spf13/cobra"
)

var whoamiCmd = &cobra.Command{
	Use: "whoami",
	Run: func(cmd *cobra.Command, args []string) {
		whoami, err := context.Whoami()
		if err != nil {
			logrus.Fatal(err)
		}
		fmt.Println(whoami)
	},
}

func init() {
	rootCmd.AddCommand(whoamiCmd)
}
