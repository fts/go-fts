package fts

import (
	"bytes"
	"crypto/tls"
	"github.com/Sirupsen/logrus"
	"gitlab.cern.ch/flutter/go-proxy"
	"io/ioutil"
	"net/http"
	"net/url"
	"path"
)

// Context holds info to interact with FTS
type Context struct {
	Endpoint          string
	UserCert, UserKey string
	Insecure          bool
	CaPath            string
	LoadCRLs          bool

	initialized bool
	transport   http.Transport
	client      http.Client
	baseurl     *url.URL
}

// initClient initializes the http client
func (c *Context) initClient() error {
	if c.initialized {
		return nil
	}

	roots, err := proxy.LoadCAPath(c.CaPath, c.LoadCRLs)
	if err != nil {
		return err
	}

	c.transport.TLSClientConfig = &tls.Config{
		RootCAs:            roots.CertPool,
		InsecureSkipVerify: c.Insecure,
	}
	c.client.Transport = &c.transport
	c.baseurl, err = url.Parse(c.Endpoint)

	cert, err := tls.LoadX509KeyPair(c.UserCert, c.UserKey)
	if err == nil {
		c.transport.TLSClientConfig.Certificates = []tls.Certificate{cert}
	}

	c.initialized = true
	return err
}

// buildURL returns the full URL for the given path
func (c *Context) buildURL(subpath string) string {
	newUrl := *c.baseurl
	newUrl.Path = path.Join(newUrl.Path, subpath)
	return newUrl.String()
}

// Get performs a low level HTTP GET
func (c *Context) Get(path string) ([]byte, error) {
	if err := c.initClient(); err != nil {
		return nil, err
	}

	url := c.buildURL(path)
	logrus.Debug("GET ", url)

	resp, err := c.client.Get(url)
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	logrus.Debug(string(body))
	return body, err
}

// Post performs a low level HTTP POST. Mime-type is json!
func (c *Context) Post(path string, mimetype string, data []byte) ([]byte, error) {
	if err := c.initClient(); err != nil {
		return nil, err
	}

	url := c.buildURL(path)
	logrus.Debug("POST ", url)
	logrus.Debug(string(data))

	resp, err := c.client.Post(url, mimetype, bytes.NewReader(data))
	if err != nil {
		return nil, err
	}
	body, err := ioutil.ReadAll(resp.Body)
	logrus.Debug(string(body))
	return body, err
}
