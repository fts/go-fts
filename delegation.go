package fts

import (
	"crypto/x509"
	"encoding/pem"
	"errors"
	"gitlab.cern.ch/flutter/go-proxy"
	"time"
)

// getProxyRequest ask the server for a proxy request
func (c *Context) getProxyRequest(dlgId string) (*x509.CertificateRequest, error) {
	raw, err := c.Get("/delegation/" + dlgId + "/request")
	if err != nil {
		return nil, err
	}
	b, _ := pem.Decode(raw)
	if err != nil {
		return nil, err
	}
	if b.Type != "CERTIFICATE REQUEST" {
		return nil, errors.New("Unexpected PEM type")
	}
	return x509.ParseCertificateRequest(b.Bytes)
}

// signRequest signs the request retrieved from the server
func (c *Context) signRequest(req *x509.CertificateRequest, lifetime time.Duration) (*proxy.X509Proxy, error) {
	x509proxy := proxy.X509Proxy{}
	err := x509proxy.DecodeFromFiles(c.UserCert, c.UserKey)
	if err != nil {
		return nil, err
	}
	return x509proxy.SignRequest(&proxy.X509ProxyRequest{Request: req}, lifetime)
}

// Delegate credentials to the remote FTS endpoint
// Return the delegation ID, or an error
func (c *Context) Delegate(lifetime time.Duration) (string, error) {
	id, err := c.Whoami()
	if err != nil {
		return "", err
	}

	request, err := c.getProxyRequest(id.DelegationID)
	if err != nil {
		return "", err
	}

	signed, err := c.signRequest(request, lifetime)
	if err != nil {
		return "", err
	}

	_, err = c.Post("/delegation/"+id.DelegationID+"/credential", "application/x-pem-file", signed.Encode())
	return id.DelegationID, err
}
