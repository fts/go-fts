package fts

import (
	"encoding/json"
	"fmt"
	"time"
)

const (
	JobTypeSessionReuse = "Y"
	JobTypeNormal       = "N"
	JobTypeMultihop     = "H"
	JobTypeMultireplica = "R"

	ChecksumEnd2End = "b"
	ChecksumTarget  = "t"
	ChecksumSource  = "s"
	ChecksumNone    = "n"
)

// Time is a wrapper around time.Time to provide our own unmarshalling
type Time struct {
	time.Time
}

// Unmarshal time with FTS format
func (t *Time) UnmarshalJSON(raw []byte) error {
	var err error
	t.Time, err = time.Parse(`"2006-01-02T15:04:05"`, string(raw))
	return err
}

// Duration is a wrapper around time.Duration to provide our own unmarshalling
type Duration struct {
	time.Duration
}

// Unmarshal duration with FTS format
func (d *Duration) UnmarshalJSON(raw []byte) error {
	var i float32
	if err := json.Unmarshal(raw, &i); err != nil {
		return err
	}
	d.Duration = time.Duration(i) * time.Second
	return nil
}

// MarshalJSON serializes Duration as a single number
func (d *Duration) MarshalJSON() ([]byte, error) {
	i := d.Duration.Seconds()
	return json.Marshal(i)
}

// File is an individual transfer within a job
type File struct {
	JobID     string `json:"job_id"`
	ID        int64  `json:"file_id"`
	FileIndex int    `json:"file_index"`
	HashedID  int    `json:"hashed_id"`

	SourceSE  string `json:"source_se"`
	DestSE    string `json:"dest_se"`
	SourceURL string `json:"source_surl"`
	DestURL   string `json:"dest_surl"`
	VOName    string `json:"vo_name"`

	State string `json:"file_state"`

	UserFilesize int64  `json:"user_filesize"`
	Checksum     string `json:"checksum"`
	Activity     string `json:"activity"`

	BringOnlineToken  *string `json:"bringonline_token"`
	SelectionStrategy string  `json:"selection_strategy"`

	Filesize   int64   `json:"filesize"`
	Throughput float32 `json:"throughput"`

	TransferHost *string  `json:"transfer_host"`
	StagingHost  *string  `json:"staging_host"`
	PID          int      `json:"pid"`
	Duration     Duration `json:"tx_duration"`

	//LogDebug bool    `json:"log_debug"`
	LogFile *string `json:"log_file"`
	Retry   int     `json:"retry"`

	StagingStart    *Time `json:"staging_start"`
	StagingFinished *Time `json:"staging_finished"`

	StartTime  *Time `json:"start_time"`
	FinishTime *Time `json:"finish_time"`

	InternalFileParams string `json:"internal_file_params"`

	Reason string `json:"reason"`
	//Recoverable bool   `json:"recoverable"`

	Metadata *json.RawMessage `json:"file_metadata"`
}

// Job contains information about an FTS job
type Job struct {
	ID    string `json:"job_id"`
	State string `json:"job_state"`

	SourceSE string `json:"source_se"`
	DestSe   string `json:"dest_se"`
	VOName   string `json:"vo_name"`

	JobType  string `json:"job_type"`
	Priority int    `json:"priority"`

	CredID string `json:"cred_id"`
	UserDN string `json:"user_dn"`

	Overwrite      bool   `json:"overwrite_flag"`
	VerifyChecksum string `json:"verify_checksum"`

	Retry      int  `json:"retry"`
	RetryDelay int  `json:"retry_delay"`
	CancelJob  bool `json:"cancel_job"`

	SubmitHost  string `json:"submit_host"`
	SubmitTime  Time   `json:"submit_time"`
	JobFinished *Time  `json:"job_finished"`

	SourceSpaceToken string `json:"source_space_token"`
	SpaceToken       string `json:"space_token"`

	CopyPinLifetime    int  `json:"copy_pin_lifetime"`
	BringOnlineTimeout int  `json:"bring_online"`
	MaxTimeInQueue     *int `json:"max_time_in_queue"`

	InternalJobParams *string `json:"internal_job_params"`

	Reason      string           `json:"reason"`
	JobMetadata *json.RawMessage `json:"job_metadata"`

	Files []File `json:"files"`
}

// String generates a pretty string representation of the Job
func (j *Job) String() string {
	jstr := fmt.Sprintf(`Request ID: %s
Status: %s
Client DN: %s
Reason: %s
Submission time: %s
Priority: %d
VO Name: %s`, j.ID, j.State, j.UserDN, j.Reason, j.SubmitTime, j.Priority, j.VOName)

	for _, f := range j.Files {
		jstr += fmt.Sprintf(
			"\n\tFile ID: %d\n"+
				"\tStatus: %s\n"+
				"\tReason: %s\n"+
				"\tSource URL: %s\n"+
				"\tDest URL: %s", f.ID, f.State, f.Reason, f.SourceURL, f.DestURL)
	}

	return jstr
}

// String  generates a pretty string representation of the File
func (f *File) String() string {
	return fmt.Sprintf(`File ID: %d
Status: %s`, f.ID, f.State)
}

// getFiles populates the Files field of the job
func (c *Context) getFiles(job *Job) (*Job, error) {
	raw, err := c.Get("/jobs/" + job.ID + "/files")
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(raw, &job.Files)
	return job, err
}

// GetJobStatus returns the status of the job identified by the given jobID
func (c *Context) GetJobStatus(jobID string, getFiles bool) (*Job, error) {
	if err := c.initClient(); err != nil {
		return nil, err
	}
	raw, err := c.Get("/jobs/" + jobID)
	if err != nil {
		return nil, err
	}

	job := &Job{}
	err = json.Unmarshal(raw, job)
	if err != nil {
		return nil, err
	}

	if getFiles {
		return c.getFiles(job)
	}
	return job, err
}
