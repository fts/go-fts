package fts

import (
	"encoding/json"
	"time"
)

// SubmissionFile models an individual file within a submission
type SubmissionFile struct {
	Sources           []string `json:"sources"`
	Destinations      []string `json:"destinations"`
	Checksum          string   `json:"checksum"`
	Metadata          *json.RawMessage `json:"file_metadata,omitempty"`
	Filesize          int64  `json:"filesize"`
	Activity          string `json:"activity"`
	SelectionStrategy string `json:"selection_strategy"`
}

// SubmissionParams configures a job
type SubmissionParams struct {
	MaxTimeInQueue     *time.Duration   `json:"max_time_in_queue,omitempty"`
	Timeout            *time.Duration   `json:"timeout,omitempty"`
	NoStreams          int              `json:"nostreams"`
	BufferSize         int              `json:"buffer_size"`
	StrictCopy         bool             `json:"strict_copy"`
	IPv4               bool             `json:"ipv4"`
	IPv6               bool             `json:"ipv6"`
	MultiHop           bool             `json:"multihop"`
	SessionReuse       bool             `json:"reuse"`
	CopyPinLifetime    *Duration        `json:"copy_pin_lifetime,omitempty"`
	BringOnlineTimeout *Duration        `json:"bring_online,omitempty"`
	Spacetoken         string           `json:"spacetoken,omitempty"`
	SourceSpacetoken   string           `json:"source_spacetoken,omitempty"`
	Retry              int              `json:"retry"`
	RetryDelay         Duration         `json:"retry_delay"`
	Priority           int              `json:"priority"`
	Overwrite          bool             `json:"overwrite"`
	VerifyChecksum     string           `json:"verify_checksum,omitempty"`
	Metadata           *json.RawMessage `json:"job_metadata,omitempty"`
	Credential         *string          `json:"credential,omitempty"`
	IdGenerator        *string          `json:"id_generator,omitempty"`
	SID                *string          `json:"sid,omitempty"`
	S3Alternate        *bool            `json:"s3alternate,omitempty"`
}

// Submission groups a set of files with a common set of parameters
type Submission struct {
	Files    []SubmissionFile `json:"files"`
	Params   SubmissionParams `json:"params"`
}

// SubmissionResponse is what the server returns after a submission
type SubmissionResponse struct {
	JobID string `json:"job_id"`
}

// Submit a job to FTS
func (c *Context) Submit(job *Submission) (string, error) {
	raw, err := json.Marshal(job)
	if err != nil {
		return "", err
	}
	rawResp, err := c.Post("/jobs", "application/json", raw)
	if err != nil {
		return "", err
	}
	resp := SubmissionResponse{}
	err = json.Unmarshal(rawResp, &resp)
	if err != nil {
		return "", err
	}
	return resp.JobID, nil
}
