package fts

import (
	"encoding/json"
	"fmt"
	"strings"
)

// ID contains the authentication information as seen by FTS
type ID struct {
	DNs          []string `json:"dn"`
	VoIDs        []string `json:"vos_id"`
	Roles        []string `json:"roles"`
	DelegationID string   `json:"delegation_id"`
	UserDN       string   `json:"user_dn"`
	//Level        map[string]string `json:"level"`
	IsRoot   bool     `json:"is_root"`
	BaseID   string   `json:"base_id"`
	Vos      []string `json:"vos"`
	VOMSCred []string `json:"voms_cred"`
	Method   string   `json:"certificate"`
}

// String generates a pretty string representation of ID
func (id *ID) String() string {
	strs := []string{
		fmt.Sprint("User DN: ", id.UserDN),
	}

	for _, vo := range id.Vos {
		strs = append(strs, fmt.Sprint("VO: ", vo))
	}
	for _, void := range id.VoIDs {
		strs = append(strs, "VO id: ", void)
	}

	strs = append(strs,
		fmt.Sprint("Delegation id: ", id.DelegationID),
		fmt.Sprint("Base id: ", id.BaseID),
	)
	return strings.Join(strs, "\n")
}

// Whoami returns information about how does the endpoint sees the client
func (c *Context) Whoami() (*ID, error) {
	raw, err := c.Get("/whoami")
	if err != nil {
		return nil, err
	}
	id := &ID{}
	err = json.Unmarshal(raw, id)
	return id, err
}
